#include <ncurses.h>
#include <stdlib.h>
#include <time.h>

typedef struct position {
	int col;
	int row;
} position;

typedef enum direction {
	UP, 
	RIGHT, 
	DOWN, 
	LEFT 
} direction;

void initSnake(position snakeBody[], int length, int row, int col)
{
	for (int i = 0; i < length; i++) {
		snakeBody[i].row = row;
		snakeBody[i].col = col - i;
	}
}

void renderSnake(position snakeBody[], int length)
{
	for (int i = 0; i < length; i++) {
		mvaddch(snakeBody[i].row, snakeBody[i].col, 'O');
	}
}

void moveSnake(position snakeBody[], int length, direction dir)
{
	position prevPos = snakeBody[0];

	switch (dir) {
		case UP:
			snakeBody[0].row--;
			break;
		case RIGHT:
			snakeBody[0].col++;
			break;
		case DOWN:
			snakeBody[0].row++;
			break;
		case LEFT:
			snakeBody[0].col--;
			break;
	}

	for (int i = 1; i < length; i++) {
		position newPos = prevPos;
		prevPos = snakeBody[i];
		snakeBody[i] = newPos;
	}
}

bool hitFood(position snake[], int snakeLength, position food[], int foodLength)
{
	for (int i = 0; i < foodLength; i++) {
		if (food[i].col == snake[0].col && food[i].row == snake[0].row) {
			food[i].col = -1;
			food[i].row = -1;
			return true;
		}
	}

	return false;
}

bool snakeCrash(position snakeBody[], int length, int boundsWidth, int boundsHeight)
{
	position head = snakeBody[0];
	if (head.row < 0) {
		return true;
	}
	if (head.col < 0) {
		return true;
	}
	if (head.row >= boundsHeight) {
		return true;
	}
	if (head.col >= boundsWidth) {
		return true;
	}

	for (int i = 0; i < length; i++) {
		for (int j = 0; j < length; j++) {
			if (i != j && snakeBody[i].col == snakeBody[j].col && snakeBody[i].row == snakeBody[j].row) {
				return true;
			}
		}
	}

	return false;
}

void initFoodCountainer(position food[], int length)
{
	for (int i = 0; i < length; i++) {
		food[i].col = -1;
		food[i].row = -1;
	}
}

void spawnFood(position food[], int length, int boundsWidth, int boundsHeight)
{
	for (int i = 0; i < length; i++) {
		if (food[i].col == -1 && food[i].row == -1) {
			food[i].col = rand() % boundsWidth;
			food[i].row = rand() % boundsHeight;
			break;
		}
	}
}

void renderFood(position food[], int length)
{
	for (int i = 0; i < length; i++) {
		if (food[i].col != -1 && food[i].row != -1) {
			mvaddch(food[i].row, food[i].col, 'X');
		}
	}
}

int runGame(int winHeight, int winWidth)
{
	int score = 0;

	struct timespec sleepTime;
	sleepTime.tv_sec = 0;
	sleepTime.tv_nsec = 200 * 1000 * 1000;

	const int MAX_SNAKE_LENGTH = 100;
	int snakeLength = 5;
	position snakeBody[MAX_SNAKE_LENGTH];
	initSnake(snakeBody, snakeLength, winHeight / 2, winWidth / 2);
	direction snakeDirection = RIGHT;
	sleepTime.tv_nsec = (200 * 1000 * 1000) - (snakeLength * 1000 * 1000);

	const int MAX_FOOD = 10;
	position food[MAX_FOOD];
	initFoodCountainer(food, MAX_FOOD);
	int numFood = 0;

	bool running = true;
	while (running == true) {
		erase();
		renderFood(food, MAX_FOOD);
		renderSnake(snakeBody, snakeLength);
		refresh();

	 	nanosleep(&sleepTime, NULL);

	 	int c = getch();
		switch (c) {
		case KEY_UP:
			snakeDirection = UP;
			break;
		case KEY_RIGHT:
			snakeDirection = RIGHT;
			break;
		case KEY_DOWN:
			snakeDirection = DOWN;
			break;
		case KEY_LEFT:
			snakeDirection = LEFT;
			break;
		case 'q':
			running = false;
			break;
		default:
			break;
		}

		if (hitFood(snakeBody, snakeLength, food, MAX_FOOD)) {
			snakeLength++;
			numFood--;
			score++;
			sleepTime.tv_nsec = (200 * 1000 * 1000) - (snakeLength * 1000 * 1000);
		}

		moveSnake(snakeBody, snakeLength, snakeDirection);

		if (snakeCrash(snakeBody, snakeLength, winWidth, winHeight) == true) {
			running = false;
		}

		if (numFood == 0 || (rand() % 100) < 3) {
			spawnFood(food, MAX_FOOD, winWidth, winHeight);
			numFood++;
		}
	}

	return score;
}

int main()
{
	initscr();
	cbreak();
	noecho();
	keypad(stdscr, true);
	curs_set(0);
	srand(time(NULL));

	int winWidth;
	int winHeight;
	getmaxyx(stdscr, winHeight, winWidth);

	bool quit = false;
	while (quit == false) {
		nodelay(stdscr, 1);

		int score = runGame(winHeight, winWidth);
		mvprintw(5, 5, "Score: %d", score);
		mvaddstr(6, 5, "Try again? (y/n)");

		nodelay(stdscr, 0);
		int c;
		do {
			c = getch();
			if (c == 'n' || c == 'q') {
				quit = true;
			}
		} while (c != 'n' && c != 'y' && c != 'q');
	}

	endwin();
	return 0;
}